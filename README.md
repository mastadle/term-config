# Personal terminal configuration
Contains vim and tmux configuration

Use [Hack](https://github.com/source-foundry/Hack) font. It has an unprecedented number of glyphs.
For use with the Starship prompt, make sure to install the corresponding [Nerdfont](https://github.com/ryanoasis/nerd-fonts)

[Monaspace Argon Nerdfont](https://www.nerdfonts.com/font-downloads) is an interesting alternative.
It uses texture healing and ligatures, which is not supported by Alacritty and might slow down other Terminal emulators.
Monaspace doesn't have such a rich set of glyphs as Hack

## Setup
Checkout this repo to `$HOME`.
In `$HOME`, create a few symlinks:
### VIM
```sh
cp ~/term-config/vim-plug/plug.vim ~/.vim/autoload/plug.vim
ln -s ~/term-config/vim/init.vim ~/.vimrc
ln -s ~/term-config/vim/coc.vim ~/.vim/coc.vim
ln -s ~/term-config/vim/coc-settings.json ~/.vim/coc-settings.json
```
### Neovim
```sh
cp ~/term-config/vim-plug/plug.vim ~/.local/share/nvim/site/autoload/plug.vim
ln -s ~/term-config/vim ~/.config/nvim
```

### tmux
```sh
ln -s ~/term-config/.tmux.conf ~/.tmux.conf
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```
In tmux, use prefix+I to install plugins and prefix+U to update

### fish
```sh
ln -s ~/term-config/fish/config.fish ~/.config/fish/config.fish
```

Fisher (package manager for fish shell)
```sh
curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
```
[Bass](https://github.com/edc/bass) let's you use tools which produce output for Bash
```sh
fisher install edc/bass
```

A reasonable node version manager:
```sh
fisher install jorgebucaran/nvm.fish
nvm install latest
```

### Bash

Node version manager:
```sh
git clone https://github.com/nvm-sh/nvm.git .nvm

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
nvm install latest
```

### SystemVerilog language server
Run
```sh
npm install -g @imc-trading/svlangserver
```
