-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This will hold the configuration.
local config = {}
if wezterm.config_builder then
	wezterm.config_builder()
end

-- This is where you actually apply your config choices

-- For example, changing the color scheme:
config.color_scheme = 'Solarized Dark - Patched'

config.font = wezterm.font('Hack Nerd Font Mono')
config.font_size = 14
config.keys = {
			-- Requires acomagu/fish-osc133 to work
      -- { key = "UpArrow",   mods = "SHIFT", action = wezterm.action.ScrollToPrompt(-1) },
      -- { key = "DownArrow", mods = "SHIFT", action = wezterm.action.ScrollToPrompt(1) },
      { key = "UpArrow",   mods = "SHIFT", action = wezterm.action.ScrollByPage(-0.5) },
      { key = "DownArrow", mods = "SHIFT", action = wezterm.action.ScrollByPage(0.5) },
    }

if package.config:sub(1,1) == '\\' then
	-- config.default_prog = { 'pwsh.exe', '-NoLogo' }

	config.default_domain = 'WSL:Debian'

	config.launch_menu = {
		{

			-- Optional label to show in the launcher. If omitted, a label
			-- is derived from the `args`

			label = 'Local Powershell',

			-- The argument array to spawn.  If omitted the default program
			-- will be used as described in the documentation above
			args = { 'pwsh.exe' },

			domain = { DomainName = 'local' }


			-- You can specify an alternative current working directory;
			-- if you don't specify one then a default based on the OSC 7
			-- escape sequence will be used (see the Shell Integration
			-- docs), falling back to the home directory.
			-- cwd = "/some/path"

			-- You can override environment variables just for this command
			-- by setting this here.  It has the same semantics as the main
			-- set_environment_variables configuration option described above
			-- set_environment_variables = { FOO = "bar" },

		},
	}
end

-- and finally, return the configuration to wezterm
return config
