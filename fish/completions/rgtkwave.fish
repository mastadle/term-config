function __rsync_remote_target
    commandline -ct | string match -r '.*::?(?:.*/)?' | string unescape
end

set -l new_escaping # has an element if the new escaping style introduced in 3.2.4 is required

set -l rsync_ver (rsync --version |
                  string replace -rf '^rsync +version\D+([\d.]+) .*' '$1' |
                  string split .)

if test "$rsync_ver[1]" -gt 3 2>/dev/null
    or test "$rsync_ver[1]" -eq 3 -a "$rsync_ver[2]" -gt 2 2>/dev/null
    or test "$rsync_ver[1]" -eq 3 -a "$rsync_ver[2]" -eq 2 -a "$rsync_ver[3]" -gt 3 2>/dev/null
    set new_escaping 1
end

complete -c rgtkwave -d "Remote path" -n "commandline -ct | string match -q '*:*'" -xa "
(
	# Prepend any user@host:/path information supplied before the remote completion.
        __rsync_remote_target
)(
	# Get the list of remote files from the specified rsync server.
        rsync --list-only (__rsync_remote_target) 2>/dev/null | string replace -r '^d.*' '\$0/' |
        string replace -r '(\S+\s+){4}' '' |
        string match --invert './' $(set -q new_escaping[1]; or echo ' | string escape -n'; echo)
)
"
