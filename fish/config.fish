if status --is-interactive
    if not command -q nvim
        set -x TERM xterm-256color
    end
    if command -q keychain
        and not ssh-add -l &> /dev/null
        if test -f ~/.ssh/id_ed25519
            keychain --quiet --nogui --agents ssh id_ed25520
        end
        if test -f ~/.ssh/id_rsa
            keychain --quiet --nogui --agents ssh id_rsa
        end
        function clear_keychain --on-event fish_exit
            if test (ps -U $USER -o comm --no-headers | grep fish | wc -l) -eq 1
                keychain --clear
            end
        end
    end
    test -e ~/.iterm2_shell_integration.fish ; and source ~/.iterm2_shell_integration.fish ; or true

    if type -q nvm
        nvm --silent use lts/iron
    end

    set local_bin ~/.local/bin
    if not [ -d $local_bin ]
        mkdir -p $local_bin
    end
    fish_add_path -g $local_bin
    if not command -q starship
        curl -Ss https://starship.rs/install.sh | sh -s -- --bin-dir $local_bin
    end
    starship init fish | source

    if not command -q rg
        if command -q curl
            set rg_version "14.1.1"
            curl -L "https://github.com/BurntSushi/ripgrep/releases/download/$rg_version/ripgrep-$rg_version-x86_64-unknown-linux-musl.tar.gz" --output "/tmp/ripgrep-$rg_version.tar.gz"
            tar -C /tmp -xf ripgrep-$rg_version.tar.gz
            mv /tmp/ripgrep-$rg_version/rg $local_bin/
        end
    end

    if [ -d /scratch/mastadle/pyenv ]
        set -gx PYENV_ROOT /scratch/mastadle/pyenv
        fish_add_path -g "$PYENV_ROOT/bin"
        pyenv init - | source
        pyenv shell (pyenv latest 3)
    end

    bind \cw backward-kill-word

    if test -e $USER/.local/share/fish/config_local.fish
        source $USER/.local/share/fish/config_local.fish
    end
    set -x GPG_TTY (tty)
end

begin
    set -l HOSTNAME (hostname)
    if test -f ~/.keychain/$HOSTNAME-fish
       source ~/.keychain/$HOSTNAME-fish
    end
end
if command -q batcat
    abbr -a -- bat batcat
end
for xilinx_path in "/opt/Xilinx" "/scratch/installs/xilinx"
    for xilinx_version in "2023.2" "2022.2" "2021.2"
        if [ -d $xilinx_path/Vitis/$xilinx_version ]
            fish_add_path -g "$xilinx_path/Vitis/$xilinx_version/bin"
            fish_add_path -g "$xilinx_path/Vitis/$xilinx_version/gnu/aarch32/lin/gcc-arm-none-eabi/bin/"
            # fish_add_path -g "$xilinx_path/Vitis/$xilinx_version/gnu/aarch32/lin/gcc-arm-linux-gnueabi/bin/"
            fish_add_path -g "$xilinx_path/Vivado/$xilinx_version/bin"
            set found_vitis 1
            break
        end
    end
    [ $found_vitis ] && break
end
