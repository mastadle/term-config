function rgtkwave 
  argparse --min-args 1 --max-args 2 'h/help' 'u/update' 'f/foreground' -- $argv
  or set _flag_help

  if set -ql _flag_help
    echo "rgtwkave [-h/--help -u/--update -f/foreground] source_fst_file [gtkw_file]"
    echo "Download fst file from source_fst_file with rsync and corresponding gtkw savefile"
    echo "If gtkw_file is not specified, a probably name will be generated from source_fst_file"
    echo " For example, with */trace.fst rsync will look for a savefile called trace.gtkw and trace.sav"
    echo " -u/--update - only call rsync and don't start gtkwave in the background"
    echo " -f/--foreground - launch gtkwave in the foreground instead of in the background"
    return 1
  end
  set source (dirname $argv[1])/
  set fstfile (basename $argv[1])
  if test (count $argv) -eq 1
    set gtkwfilename (string replace -r '.[^.]*$' '' $fstfile | sed 's/000/001/')
  else
    set gtkwfilename (string replace -r '.[^.]*$' '' $argv[2])
  end
  set filterrules --filter "+ $fstfile" --filter "+ $fstfile.hier"
  set -a filterrules --filter "S $gtkwfilename.gtkw" --filter "S $gtkwfilename.sav"
  rsync -ahz --progress \
    $filterrules \
    --filter  "- *" \
    $source ./

  if not set -ql _flag_update
    if test -f $gtkwfilename.sav
      set gtkwfile $gtkwfilename.sav
    else
      set gtkwfile $gtkwfilename.gtkw
    end
    if set -ql _flag_foreground
      switch (uname)
        case Darwin
          /Applications/gtkwave.app/Contents/Resources/bin/gtkwave $fstfile $gtkwfile
        case Linux
          GDK_BACKEND=x11 gtkwave $fstfile $gtkwfile
        case '*'
          gtkwave $fstfile $gtkwfile
      end
    else
      switch (uname)
        case Darwin
          /Applications/gtkwave.app/Contents/Resources/bin/gtkwave $fstfile $gtkwfile &
        case Linux
          GDK_BACKEND=x11 gtkwave $fstfile $gtkwfile &
        case '*'
          gtkwave $fstfile $gtkwfile &
      end
    end
  end
end
