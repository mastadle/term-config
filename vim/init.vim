if has("win32")
	" Note, you need to empty the file Git\etc\motd
	" to get rid of the 'Welcome to Git' message
	set shell=cmd.exe
endif

if has('nvim')
	let $BASE_CONFIG = stdpath('config')
	let $BASE_DATA = stdpath('data')
else
   if has('win32')
      let $BASE_CONFIG = '$HOME/vimfiles'
      let $BASE_DATA = '$HOME/vimfiles'
   else
      let $BASE_CONFIG = '$HOME/.vim'
      let $BASE_DATA = '$HOME/.vim'
   endif
endif
if filereadable($BASE_CONFIG.'/init-local.vim')
	source $BASE_CONFIG/init-local.vim
endif

set nocompatible

call plug#begin($BASE_DATA.'/plugged')

" Plug 'preservim/nerdtree'

" Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/vim-easy-align'

" Normal mode align
nmap ga <Plug>(EasyAlign)
" Visual mode align
xmap ga <Plug>(EasyAlign)

if has('nvim-0.6.0')
	if has('nvim-0.7.0')
		if has('nvim-0.8.0')
			if has('nvim-0.9.0')
				Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
			else
				Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate' , 'commit': 'v0.8.1'}
			endif
		else
			Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate' , 'commit': 'v0.7.2'}
		endif
	else
		Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate', 'commit': 'bc25a6a5c4fd659bbf78ba0a2442ecf14eb00398'}
	endif
	Plug 'nvim-lua/plenary.nvim'
else
	Plug 'sheerun/vim-polyglot'
endif
if has('nvim-0.5.0')
	Plug 'mfussenegger/nvim-dap'
	Plug 'mfussenegger/nvim-dap-python'
	nnoremap <silent> <F5> <Cmd>lua require'dap'.continue()<CR>
	nnoremap <silent> <F10> <Cmd>lua require'dap'.step_over()<CR>
	nnoremap <silent> <F11> <Cmd>lua require'dap'.step_into()<CR>
	nnoremap <silent> <F12> <Cmd>lua require'dap'.step_out()<CR>
	nnoremap <silent> <leader>db <Cmd>lua require'dap'.toggle_breakpoint()<CR>
	nnoremap <silent> <leader>dB <Cmd>lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>
	nnoremap <silent> <leader>lp <Cmd>lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>
	nnoremap <silent> <leader>dr <Cmd>lua require'dap'.repl.open()<CR>
	nnoremap <silent> <leader>dl <Cmd>lua require'dap'.run_last()<CR>
	nnoremap <silent> <leader>dn :lua require('dap-python').test_method()<CR>
else
	Plug 'puremourning/vimspector'
endif
if has('nvim')
	Plug 'averms/black-nvim', {'do': ':UpdateRemotePlugins'}
	if !has('nvim-0.9.0')
		Plug 'gpanders/editorconfig.nvim'
	endif
else
	Plug 'editorconfig/editorconfig-vim'
	Plug 'tpope/vim-sensible'
endif
Plug 'joshdick/onedark.vim'
Plug 'NLKNguyen/papercolor-theme'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'tpope/vim-fugitive'
Plug 'shumphrey/fugitive-gitlab.vim'

" Plug 'preservim/nerdcommenter'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-vinegar'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-sleuth'

Plug 'roxma/vim-tmux-clipboard'
Plug 'christoomey/vim-tmux-navigator'

Plug 'lervag/vimtex'
if has("linux")
	let g:vimtex_view_method = 'zathura'
else
	let g:vimtex_view_method = 'skim'
endif
let g:vimtex_grammar_textidote = {
			\ 'jar': '/usr/share/java/textidote.jar',
			\ 'args': '--read-all',
			\}
nmap <Leader>ld compiler
set spelllang=en_gb
set spellfile=$BASE_CONFIG/spell/en.utf-8.add
let g:vimtex_compiler_method = 'latexmk'

" Plug 'wsdjeg/vim-fetch'
"
if !has('nvim-0.9.0')
	if v:version > 900 || has('nvim-0.8.0')
		Plug 'neoclide/coc.nvim', { 'branch': 'release' }
	endif
else
	Plug 'neovim/nvim-lspconfig'
	Plug 'p00f/clangd_extensions.nvim'
endif

if has('nvim-0.10.0')
	Plug '3rd/image.nvim'
	Plug 'nvim-neorg/lua-utils.nvim'
	Plug 'nvim-neotest/nvim-nio'
	Plug 'MunifTanjim/nui.nvim'
	Plug 'pysan3/pathlib.nvim'
	Plug 'nvim-neorg/neorg'
endif

call plug#end()

set mouse=nvi
if !has('nvim')
	if has("mouse_sgr")
		set ttymouse=sgr
	else
		" tmux is better than screen
		set ttymouse=xterm2
	endif
endif

set termguicolors

let g:airline_powerline_fonts = 1

let g:airline_theme='papercolor'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
if !exists('g:airline_symbols')
let g:airline_symbols = {}
endif
" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.colnr = ' |:'
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ' :'
let g:airline_symbols.maxlinenr = '☰ '
let g:airline_symbols.dirty='⚡'

syntax on
set number
set laststatus=2
set wildmode=longest:full,full
set ignorecase
set smartcase
set hls
" Preview effects of command incrementally (e.g. :substitute). Neovim only.
if has('nvim')
	set inccommand=nosplit
endif
set tabstop=2
set linebreak
set signcolumn=yes

let g:termdebug_wide = 1
nnoremap <leader>bc :<C-u>bp<bar>bd#<cr>


set background=light
colorscheme PaperColor

let g:fugitive_gitlab_domains = ['ssh://gitlab.phys.ethz.ch', 'ssh://gitlab.ethz.ch']

let g:gitlab_api_keys = {'gitlab.phys.ethz.ch': 'QQxxTyAb4yfGg9bFsHL7'}

if !has('nvim-0.9.0')
	if v:version > 900 || has('nvim-0.8.0')

		let g:coc_global_extensions = [
					\'coc-json', 'coc-git', 'coc-clangd',
					\'coc-cmake', 'coc-pyright', 'coc-tsserver',
					\'coc-vimtex']

		" nnoremap <silent><nowait> <space>e :<C-u>CocCommand explorer --sources=file+<CR>
		" nmap <Leader>er <Cmd>call CocAction('runCommand', 'explorer.doAction', 'closest', ['reveal:0'], [['relative', 0, 'file']])<CR>

		source $BASE_CONFIG/coc.vim
	endif
else
	source $BASE_CONFIG/lsp.lua
endif

nnoremap <silent><nowait> <space>e :<C-u>Lexplore<CR>:let b:netrw_browse_split=4<CR>
let g:netrw_banner = 0
" let g:netrw_browse_split = 4
let g:netrw_liststyle = 3
let g:netrw_altv = 1
let g:netrw_winsize = 20

nmap <Leader>-v <Plug>VinegarVerticalSplitUp
nmap <Leader>-h <Plug>VinegarSplitUp

command! -bang -nargs=* Rgn
			\ call fzf#vim#grep(
			\   'rg --column --no-ignore --line-number --no-heading --color=always --smart-case -- '.shellescape(<q-args>), 1,
			\   fzf#vim#with_preview(), <bang>0)

runtime ftplugin/man.vim
filetype plugin indent on

" python3 from powerline.vim import setup as powerline_setup
" python3 powerline_setup()
" python3 del powerline_setup

" autocmd VimEnter * NERDTree | wincmd p

" Close the tab if NERDTree is the only window remaining in it.
" autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

if has('nvim')
	" Terminal settings
	" cc for commandline, cs for split first, ce to exit
	:tnoremap <ESC> <C-\><C-n>
	if has("win32")
		" Note, you need to empty the file Git\etc\motd
		" to get rid of the 'Welcome to Git' message
		nnoremap <Leader>ch :term<CR>adoskey.cmd<CR>cmd.exe /c "C:\\Progra~2\Git\bin\bash.exe --login -i"<CR>clear<CR>
		nnoremap <Leader>cs :sp<CR>:wincmd j<CR>:term<CR>adoskey.cmd<CR>cls<CR>cmd.exe /c "C:\\Progra~2\Git\bin\bash.exe --login -i"<CR>clear<CR>
		nnoremap <Leader>chd :term<CR>adoskey.cmd<CR>cls<CR>
		nnoremap <Leader>csd :sp<CR>:wincmd j<CR>:term<CR>adoskey.cmd<CR>cls<CR>
		nnoremap <Leader>cc :sp<CR>:wincmd j<CR>:term<CR><C-w>Jadoskey.cmd<CR>cls<CR>cmd.exe /c "C:\\Progra~2\Git\bin\bash.exe --login -i"<CR>clear<CR>
	else
		nnoremap <Leader>ch :term<CR>A
		nnoremap <Leader>cs :sp<CR>:wincmd j<CR>:term<CR>A
		nnoremap <Leader>cc :sp<CR>:wincmd j<CR>:term<CR><C-w>JA
	endif

	augroup TerminalMappings
		autocmd!
		if has("win32")
			autocmd TermOpen * nnoremap <buffer> <C-E> aexit<CR>exit<CR>
			autocmd TermOpen * tnoremap <buffer> <C-E> exit<CR>exit<CR>
		else
			autocmd TermOpen * nnoremap <buffer> <C-E> aexit<CR>
			autocmd TermOpen * tnoremap <buffer> <C-E> exit<CR>
		endif
	augroup END
endif

" Make the split bigger
" nnoremap <Leader>b 8<C-W>+<CR>
" nnoremap <Leader>B 50<C-W>+<CR>
" Make the split wider
nnoremap <Leader>w 8<C-W>><CR>
nnoremap <Leader>W 56<C-W>><CR>

map tw :call IwhiteToggle()<CR>
function! IwhiteToggle()
 if &diffopt =~ 'iwhite'
	 set diffopt-=iwhite
 else
	 set diffopt+=iwhite
 endif
endfunction

if has('nvim-0.6.0')
	source $BASE_CONFIG/treesitter.lua
endif
if has('nvim-0.10.0')
	source $BASE_CONFIG/neorg.lua
endif
if has('nvim-0.5.0')
	" Set $DEBUGPYVENV in init-local.vim in your BASE_CONFIG folder
	" lua require('dap-python').setup(os.getenv("DEBUGPYVENV"))
	" make sure debugpy is installed
	" lua require('dap-python').setup('/usr/bin/python')
	lua require('dap-python').setup('python3')
endif

fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

nnoremap <Leader>w :call TrimWhitespace()<CR>
