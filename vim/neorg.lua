require('neorg').setup {
        -- Tell Neorg what modules to load
        load = {
            ["core.defaults"] = {}, -- Load all the default modules
            ["core.concealer"] = {}, -- Allows for use of icons
            ["core.latex.renderer"] = {},
            ["core.dirman"] = { -- Manage your directories with Neorg
                config = {
                    workspaces = {
                        notes = "~/Documents/notes"
                    },
		    default_workspace = "notes",
                },
            },
        },
    }

vim.wo.foldlevel = 99
vim.wo.conceallevel = 2
