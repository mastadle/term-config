require'lspconfig'.pyright.setup{}
require'lspconfig'.ltex.setup{
    -- Use :LspStart to start instead
    autostart = false,
    settings = {
	ltex = {
	    dictionary = {
		['en-GB'] = {
"upconversion", "Upconversion", "upconverted", "upconvert", "halfband"
		},
	    },
	},
    },
    on_init = function(client, bufnr)
	_ = bufnr
	local spell_file_name = vim.fn.stdpath("config") .. 'spell/en.utf-8.add'
	local spell_file = io.open(spell_file_name, 'r')
	if spell_file then
	    local dict = client.config.settings.ltex.dictionary['en-GB']
	    for line in spell_file:lines() do
		table.insert(dict, line)
	    end
	    spell_file:close()
	end
    end,
}
require'lspconfig'.clangd.setup{}
require'lspconfig'.svlangserver.setup{}
