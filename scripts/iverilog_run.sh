#!/bin/bash

run_gtkwave=1
args=()

while [[ $# -gt 0 ]]; do
	case $1 in
		-h|--help)
			echo "Use -s or --sim-only to execute testbench with displaying it in gtkwave"
			echo "All other arguments are forwarded to iverilog."
			echo "Final argument must be the testbench filename"
			exit 0
			;;
		-s|--sim-only)
			run_gtkwave=0
			shift
			;;
		*)
			args+=("$1")
			shift
			;;
	esac
done

file=$(realpath ${args[-1]})
fileBasename=$(filename ${file})
fileBasenameNoExtension=${fileBasename%.*}
fileDirname=$(dirname ${file})

unset args[-1]
iverilog -g2012 -Wall "${args[@]}" -I${fileDirname} -o ${fileDirname}/icarus_compile/000_${fileBasenameNoExtension}.compiled ${file}
if [[ $? -eq 0 ]]; then
	vvp ${fileDirname}/icarus_compile/000_${fileBasenameNoExtension}.compiled -fst
else
	exit 1
fi
if [[ $run_gtkwave -eq 1 ]]; then
	gtkwave ${fileDirname}/icarus_compile/000_${fileBasenameNoExtension}.fst ${fileDirname}/icarus_compile/001_${fileBasenameNoExtension}.sav
fi
