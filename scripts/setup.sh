#!/bin/bash
mkdir -p ~/.vim/autoload
ln -s ~/term-config/vim-plug/plug.vim ~/.vim/autoload/plug.vim
ln -s ~/term-config/vim/init.vim ~/.vimrc
ln -s ~/term-config/vim/coc.vim ~/.vim/coc.vim
ln -s ~/term-config/vim/coc-settings.json ~/.vim/coc-settings.json
mkdir -p ~/.local/share/nvim/site/autoload
ln -s ~/term-config/vim-plug/plug.vim ~/.local/share/nvim/site/autoload/plug.vim
ln -s ~/term-config/vim ~/.config/nvim
mkdir -p ~/.tmux/plugins
ln -s ~/term-config/tpm/ ~/.tmux/plugins/tpm
ln -s ~/term-config/.tmux.conf ~
